package com.rave.fragmentpoc

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.fragment.app.Fragment
import com.rave.fragmentpoc.ui.theme.VibrantGreen

/**
 * Fragment that holds the initial vibrant colored screen.
 *
 * @constructor Create empty Main fragment
 */
class MainFragment : Fragment() {
    @Composable
    fun MainScreen() {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = VibrantGreen
        ) {}
    }
}
