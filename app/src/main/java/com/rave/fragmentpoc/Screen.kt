package com.rave.fragmentpoc

/**
 * Contains the routes for all of the screens.
 *
 * @property route
 * @constructor Create empty Screen
 */
sealed class Screen(val route: String) {

    /**
     * Object containing mainscreen route.
     *
     * @constructor Create empty Main screen
     */
    object MainScreen : Screen("main_screen")
}
